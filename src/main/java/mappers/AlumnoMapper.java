package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import model.Alumno;

public interface AlumnoMapper {
	
	public Alumno selectAlumno (int id);
	
	@Select("select * from alumno")
	public List<Alumno> selectAllAlumnos();
	
	@Insert("insert into alumno (id,nombre, apellidos, dni, anyo_nacimiento) values (#{param1}, #{param2}, #{param3}, #{param4}, #{param5})")
	public boolean insertAlumno(int id, String nombre, String apellidos, String dni, int anyo_nacimiento);
	
	@Select("select * from alumno where dni = #{dni}")
	public List<Alumno> selectPorDni(String dni);
	
	
	
	
	
	
	
	
	
	

	
	

}
