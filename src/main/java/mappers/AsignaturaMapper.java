package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import model.Asignatura;

public interface AsignaturaMapper {
	
public Asignatura selectAsignatura (int id);
	
	@Select("select * from asignatura")
	public List<Asignatura> selectAllAsignatura();
	
	@Insert("insert into asignatura (id, nombre, horas) values (#{param1}, #{param2}, #{param3})")
	public boolean insertAsignatura(int id, String nombre, int horas);
	
	@Select("select * from asignatura where nombre = #{nombre}")
	public List<Asignatura> selectUnaAsignatura(String nombre);
	
	@Select("select asi.nombre from alumno alu join asignatura_alumno cur on alu.id = cur.id_alumno join asignatura asi on asi.id = cur.id_asignatura where dni = #{dni}")
	public List<Asignatura> selectAsignaturaAlumno(@Param("dni") String dni);
	

}
