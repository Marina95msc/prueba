package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import model.Profesor;

public interface ProfesorMapper {
	
public Profesor selectProfesor (int id);
	
	@Select("select * from profesor")
	public List<Profesor> selectAllProfesores();
	
	@Insert("insert into profesor (id,nombre, apellidos, dni, anyo_nacimiento) values (#{param1}, #{param2}, #{param3}, #{param4}, #{param5})")
	public boolean insertProfesor(int id, String nombre, String apellidos, String dni, int anyo_nacimiento);
	
	@Select("SELECT dni, nombre, apellidos, anyo_nacimiento, (2020 - anyo_nacimiento) as edad from profesor where (2020 - anyo_nacimiento)>#{edad};")
	public List<Profesor> selectRangoEdad(int edad);
	
	
	@Update("UPDATE profesor SET id_asignatura = #{asignatura} WHERE (id = #{profesor})")
	public boolean updateProfesor(@Param("profesor") int profesor, @Param("asignatura") int asignatura );
	
	/*@Select("SELECT pro.dni, pro.nombre, pro.apellidos, asi.nombre, asi.horas FROM profesor pro join asignatura asi on pro.id = asi.id;")
	public List<Profesor> selectPorAsignatura(); */
	

}
