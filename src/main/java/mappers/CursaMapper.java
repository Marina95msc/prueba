package mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import model.Cursa;

public interface CursaMapper {
	
	@Select("select * from asignatura_alumno")
	public List<Cursa> selectAllCursa();
	
	@Update("UPDATE asignatura_alumno SET id_asignatura = #{asignatura}, id_alumno = #{alumno} WHERE (id = #{cursa})")
	public boolean updateCursa(@Param("cursa") int cursa,@Param("asignatura") int asignatura, @Param("alumno") int alumno );
	
	

}
