package model;

public class Cursa {

	private int id;
	
	private int id_asignatura;
	
	private int id_alumno;


	public Cursa() {
		
		
	}
	
	public Cursa(int id, int id_asignatura, int id_alumno) {
		
		this.id = id;
		
		this.id_asignatura = id_asignatura;
		
		this.id_alumno = id_alumno;
		
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_asignatura() {
		return this.id_asignatura;
	}

	public void setId_asignatura(int id_asignatura) {
		this.id_asignatura = id_asignatura;
	}

	public int getId_alumno() {
		return this.id_alumno;
	}

	public void setId_alumno(int id_alumno) {
		this.id_alumno = id_alumno;
	}
	

}
