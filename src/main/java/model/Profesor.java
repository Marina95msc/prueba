package model;

public class Profesor {

	private int id;

	private String nombre;

	private String apellidos;

	private String dni;

	private int anyo_nacimiento;

	private int id_asignatura;
	
	public Profesor () {
		
		
	}

	public Profesor(int id, String nombre, String apellido, String dni, int anyo_nacimiento, int id_asignatura) {

		this.id = id;

		this.nombre = nombre;

		this.apellidos = apellido;

		this.dni = dni;

		this.anyo_nacimiento = anyo_nacimiento;

		this.id_asignatura = id_asignatura;

	}

	public Profesor(int id, String nombre, String apellido, String dni, int anyo_nacimiento) {

		this.id = id;

		this.nombre = nombre;

		this.apellidos = apellido;

		this.dni = dni;

		this.anyo_nacimiento = anyo_nacimiento;

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return this.dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getAnyo_nacimiento() {
		return this.anyo_nacimiento;
	}

	public void setAnyo_nacimiento(int anyo_nacimiento) {
		this.anyo_nacimiento = anyo_nacimiento;
	}

	public int getId_asignatura() {
		return this.id_asignatura;
	}

	public void setId_asignatura(int id_asignatura) {
		this.id_asignatura = id_asignatura;
	}

}
