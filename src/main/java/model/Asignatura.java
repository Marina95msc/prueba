package model;

public class Asignatura {

	private int id;

	private String nombre;

	private int horas;
	
	public Asignatura() {
		
		
	}

	public Asignatura(int id, String nombre, int horas) {

		this.id = id;

		this.nombre = nombre;

		this.horas = horas;

	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getHoras() {
		return this.horas;
	}

	public void setHoras(int horas) {
		this.horas = horas;
	}

}
