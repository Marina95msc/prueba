package colegio;

import java.io.InputStream;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import mappers.AlumnoMapper;
import model.Alumno;

public class Ejemplo {

	public static void main(String[] args) {
		
		String resource = "mybatis-config.xml";
		
		InputStream inputStream = Ejemplo.class.getClassLoader().getResourceAsStream(resource);
		
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		try (SqlSession session = sqlSessionFactory.openSession()) {
			
			AlumnoMapper mapper = session.getMapper(AlumnoMapper.class);
			
			List<Alumno> listAlumno = mapper.selectAllAlumnos();
			
			for (Alumno alumno : listAlumno) {

				System.out.println(alumno.getDni() + " " + alumno.getNombre() + " " + alumno.getApellidos() + " "
						+ alumno.getAnyo_nacimiento());
			}
			
			
			
			mapper.insertAlumno(2, "helena", "diaz", "2222222d", 1998);
			
			session.commit(); 
			
			
		}
		
		
		
		

	}

}
