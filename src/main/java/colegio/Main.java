package colegio;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import org.apache.ibatis.session.SqlSession;
import mappers.AlumnoMapper;
import mappers.AsignaturaMapper;
import mappers.CursaMapper;
import mappers.ProfesorMapper;
import model.Alumno;
import model.Asignatura;
import model.Profesor;

public class Main {

	static final String DB_URL = "jdbc:mysql://localhost/colegio?serverTimezone=UTC";

	static final String USER = "root";
	static final String PASS = "1234";

	public static void main(String[] args) throws SQLException {

		Scanner entrada = new Scanner(System.in);

		Conexion conexion = new Conexion();

		System.out.println("---------------------------------------------------------------");
		System.out.println("�Qu� opci�n quieres elegir?");
		System.out.println("1- Crear nueva asignatura");
		System.out.println("2- Crear nuevo alumno");
		System.out.println("3- Crear nuevo profesor");
		System.out.println("4- Consultar todas las asignaturas");
		System.out.println("5- Consultar todos los alumnos");
		System.out.println("6- Consultar todos los profesores");
		System.out.println("7- Buscar asignaturas por nombre ");
		System.out.println("8- Buscar alumnos por DNI");
		System.out.println("9- Buscar profesores mayores de cierta edad");
		System.out.println("10- Vincular profesores a asignaturas");
		System.out.println("11- Vincular alumnos a asignaturas");
		System.out.println("12- Consultar las asignaturas de un alumno dado su DNI");

		int numero = entrada.nextInt();

		switch (numero) {

		case 1:

			SqlSession session = conexion.Conection();

			AsignaturaMapper mapper = session.getMapper(AsignaturaMapper.class);

			mapper.insertAsignatura(3, "ingl�s", 35);

			session.commit();

			System.out.println("Se ha a�adido la asignatura");

			break;

		case 2:

			SqlSession session2 = conexion.Conection();

			AlumnoMapper mapper2 = session2.getMapper(AlumnoMapper.class);

			mapper2.insertAlumno(3, "jose", "hernandez", "333333s", 1969);

			session2.commit();

			System.out.println("Se ha a�adido el alumno");

			break;

		case 3:

			SqlSession session3 = conexion.Conection();

			ProfesorMapper mapper3 = session3.getMapper(ProfesorMapper.class);

			mapper3.insertProfesor(2, "lorena", "garcia", "33333", 1990);

			session3.commit();

			System.out.println("Se ha a�adido el profesor");

			break;

		case 4:

			SqlSession session4 = conexion.Conection();

			AsignaturaMapper mapper4 = session4.getMapper(AsignaturaMapper.class);

			List<Asignatura> listAsignatura = mapper4.selectAllAsignatura();

			System.out.println("Asignatura OK");

			for (Asignatura asignatura : listAsignatura) {

				System.out.println(asignatura.getId() + " " + asignatura.getNombre() + " " + asignatura.getHoras());

			}

			break;

		case 5:

			SqlSession session5 = conexion.Conection();

			AlumnoMapper mapper5 = session5.getMapper(AlumnoMapper.class);

			List<Alumno> listAlumno = mapper5.selectAllAlumnos();

			for (Alumno alumno : listAlumno) {

				System.out.println(alumno.getDni() + " " + alumno.getNombre() + " " + alumno.getApellidos() + " "
						+ alumno.getAnyo_nacimiento());
			}

			break;

		case 6:

			SqlSession session6 = conexion.Conection();

			ProfesorMapper mapper6 = session6.getMapper(ProfesorMapper.class);

			List<Profesor> listProfesor = mapper6.selectAllProfesores();


			for (Profesor profesor : listProfesor) {

				System.out.println(profesor.getDni() + " " + profesor.getNombre() + " " + profesor.getApellidos() + " "
						+ profesor.getAnyo_nacimiento() + " " + profesor.getId_asignatura());
			}

			break;

		case 7:

			SqlSession session7 = conexion.Conection();

			AsignaturaMapper mapper7 = session7.getMapper(AsignaturaMapper.class);
			
			
			List<Asignatura> listAsignatura2 = mapper7.selectUnaAsignatura("ingles");
			
			for (Asignatura asignatura : listAsignatura2) {

				System.out.println(asignatura.getId() + " " + asignatura.getNombre() + " " + asignatura.getHoras());

			}
			
			break;

		case 8:
			
			SqlSession session8 = conexion.Conection();

			AlumnoMapper mapper8 = session8.getMapper(AlumnoMapper.class);

			List<Alumno> listAlumno2 = mapper8.selectPorDni("1111");
			
			for (Alumno alumno : listAlumno2) {

				System.out.println(alumno.getDni() + " " + alumno.getNombre() + " " + alumno.getApellidos() + " "
						+ alumno.getAnyo_nacimiento());
			}

			break;

		case 9:
			
			SqlSession session9 = conexion.Conection();

			ProfesorMapper mapper9 = session9.getMapper(ProfesorMapper.class);

			List<Profesor> listProfesor2 = mapper9.selectRangoEdad(39);

			for (Profesor profesor : listProfesor2) {

				System.out.println(profesor.getDni() + " " + profesor.getNombre() + " " + profesor.getApellidos() + " "
						+ profesor.getAnyo_nacimiento() + " " + profesor.getId_asignatura());
			}
			

			break;

		case 10:
			
			SqlSession session10 = conexion.Conection();

			ProfesorMapper mapper10 = session10.getMapper(ProfesorMapper.class);
			
			mapper10.updateProfesor(2, 2);
			
			session10.commit();
			
			System.out.println("Se ha actualizado el profesor");

			break;

		case 11:
			
			SqlSession session11 = conexion.Conection();
			
			CursaMapper mapper11 = session11.getMapper(CursaMapper.class);
			
			mapper11.updateCursa(3, 1, 2);
			
			session11.commit();
			
			System.out.println("Se ha actualizado cursa");	

			break;

		case 12:
			
			SqlSession session12 = conexion.Conection();

			AsignaturaMapper mapper12 = session12.getMapper(AsignaturaMapper.class);
			
			List<Asignatura> listAsignatura3 = mapper12.selectAsignaturaAlumno("1111");
			
			for (Asignatura asignatura : listAsignatura3) {

				System.out.println(asignatura.getNombre());

			}

			break;
			
		default:
				
			{
				System.out.println("Introduce un n�mero v�lido");	
				
			}

		}
	}
}
