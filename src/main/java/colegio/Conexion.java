package colegio;

import java.io.InputStream;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class Conexion {
	
	public SqlSession Conection() {
		
		String resource = "mybatis-config.xml";
		
		InputStream inputStream = Ejemplo.class.getClassLoader().getResourceAsStream(resource);
		
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		
		SqlSession session = sqlSessionFactory.openSession();
		
		return session;
	}
	
}
